# -*- coding: utf-8 -*-
"""
This module parses Wiki XML corpus,performs Tokenization and store parsed data in file

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python corpusParser.py

Attributes:

Todo:
    * For module TODOs

"""
import xml.etree.cElementTree as ET
import re
# from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
# import indexGenerator as inGen
import sys
import time
import string
import sys
import io
import gc


stop_words = {"it's", 'few', "hadn't", "haven't", 'ma', 'they', 'now', 'against', 'from', 'am', 'both', "mightn't", 'in', 'your', 'our', "aren't", 'hers', 'how', 'mustn', \
"isn't", 'ours', 'is', 'up', 'off', 'has', 'just', 'be', "needn't", 'down', 'why', 'some', "you've", 'most', 'an', 'where', 'weren', 'should', "won't", 're', 'any', \
'ourselves', 'had', 'for', 'isn', 'we', 'each', 'have', 've', "doesn't", 'wouldn', "wouldn't", 'she', 'more', 'he', 'it', "should've", 'to', 'over', 'does', 'whom', \
"don't", 'theirs', 'her', 'when', 'himself', 'don', 'during', 'i', 'between', 'own', 'not', 'did', 's', 'needn', "wasn't", 'me', 'before', 'if', 't', 'didn', 'very', \
'm', 'while', 'again', 'as', 'll', "hasn't", 'him', 'wasn', 'his', 'd', 'herself', 'my', 'will', 'by', 'were', 'ain', 'their', 'was', 'out', 'under', 'o', 'haven', \
'yourself', 'about', 'can', 'shouldn', 'a', 'mightn', 'the', 'after', "you're", 'this', 'on', 'yourselves', 'what', 'shan', 'aren', 'been', 'nor', "weren't", 'and', \
'such', 'there', 'same', 'itself', 'them', 'being', 'but', 'then', "you'd", 'until', 'that', 'those', 'myself', 'with', "shan't", 'you', 'themselves', 'its', 'do',\
 'than', 'hadn', "mustn't", "you'll", "that'll", 'of', 'through', 'which', 'so', 'yours', 'are', 'couldn', 'won', "shouldn't", 'these', 'too', "couldn't", 'below', \
 'hasn', 'doing', 'above', 'all', 'y', "she's", 'only', 'doesn', 'at', 'having', 'other', 'because', 'into', 'here', 'further', "didn't", 'no', 'once', 'who', 'or'}
sn = SnowballStemmer("english")

Title_Macro = 't'
InfoBox_Macro = 'i'
Body_Macro = 'b'
Reference_Macro = 'r'
Category_Macro = 'c'
External_links_Macro = 'l'
blockDict,c = {},1
"""############################################### 
Args :
	parseTitle	: parsed text from XML <title> tag
Returns:
    Expression that returns XML tag name seperated from namespace
###############################################"""
def extractTag(str = None):
	if str == None:
		return ''
	else:
		expr = re.compile('{(.*)}(.*)').match(str)
		return expr.group(2)


def write_to_file(filename = None, mode = None,bufferString = None):

	with open(filename,mode) as fh:
		fh.write(bufferString)
		print(f"{filename} write complete")

class PageParser(object):
	"""docstring for PageParser"""
	def __init__(self,docId = None ,parseTitle = None):
		super(PageParser, self).__init__()
		self.title = parseTitle.lower()
		self.Infobox = ''
		self.body = ''
		self.Reference = ''
		self.External_links = ''
		self.Category = ''
		self.docId = docId


	"""#####################################################
	Name :	  WikiParser 
	Function :
		  	  to parse text in <text> and calls spimiInvert
	Args :
	    wikiParser : pageText
	Returns:
	    None
	##########################################################"""

	def wikiParser(self,pageText = None):
		# print(parseTitle)
		flagInfobox,flagReference,flagExternal,flagCategory,flagExtExcep = False,False,False,False,False
		infoboxStack,refStack,extStack,catStack = 0,0,0,0
		strBufferInfoBox,strBufferRef,strBufferExternal,strBufferCat,strBuffBody = '','','','',''
		outlinks = []
		for line in io.StringIO(pageText):

			line = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+','',line)
			if re.search('\{\{\s*Infobox',line):
				flagInfobox = True
				strBufferInfoBox = ''.join([strBufferInfoBox,' ',line])
				infoboxStack = 1
				# print("flag info set")
			elif re.search('==\s*references\s*==',line.lower()):
				flagReference = True
				flagExternal = False
				flagCategory = False
			elif re.search('==\s*external links\s*==',line.lower()):
				flagExternal = True
				flagReference = False
				flagCategory = False
			elif re.search('\[\[Category:|\[\[:Category:',line):
				flagCategory = True
				flagReference = False
				flagExternal = False
				exp = re.match('\[\[Category:(.+)\]\]|\[\[:Category:(.+)\]\]',line)
				if exp and exp.group(1):
					strBufferCat = ''.join([strBufferCat,' ',exp.group(1)])
				elif exp and exp.group(2):
					strBufferCat = ''.join([strBufferCat,' ',exp.group(2)])
			elif flagInfobox:
				openBr = re.findall('\[\[|\{\{',line)
				closeBr = re.findall('\]\]|\}\}',line)
				if openBr:
					infoboxStack += len(openBr)
				if closeBr:
					infoboxStack -= len(closeBr)
				if infoboxStack == 0:
					flagInfobox = False
				# outlinks.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+',line))
				# for link in outlinks:
				# 	line = re.sub(re.escape(link),' ',(line))
				strBufferInfoBox = ''.join([strBufferInfoBox,' ',line])
			elif flagReference:
				# outlinks.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+',line))
				# for link in outlinks:
				# 	line = re.sub(re.escape(link),' ',line)
				exp = re.match('\{\{(.*)\}\}',line)
				if exp and exp.group(1):
					line = exp.group(1)
				strBufferRef = ''.join([strBufferRef,' ',line])
			elif flagExternal:
				exp = re.match('\*\s*\{\{(.*)\}\}|\*\s\[(.*)\]',line)
				if exp and exp.group(1):
					line = exp.group(1)
				elif exp and exp.group(2):
					line = exp.group(2)
				strBufferExternal = ''.join([strBufferExternal,' ',line])
			else:
				# outlinks.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+',line))
				# for link in outlinks:
				# 	line = re.sub(re.escape(link),' ',(line))
				strBuffBody = ''.join([strBuffBody,' ',line])
		pageText = ''
		self.Infobox = re.sub('<.*?>','',strBufferInfoBox.lower())
		self.Reference = re.sub('<.*?>','',strBufferRef.lower())
		self.External_links = re.sub('<.*?>','',strBufferExternal.lower())
		self.body = re.sub('<.*?>','',strBuffBody.lower())

		self.Category = re.sub('<.*?>','',strBufferCat.lower())

		self.spimiInvert(blockSizeLimit)

	"""##################################################################
	Name : parseXML will parse XML dump and call parseWiki() to parse text in <text>
	Args :
	    filename : User cli args
	Returns:
	    None
	###################################################################"""

	def spimiInvert(self, blockSizeLimit = None):
	
		global blockDict
		typesOfTokens = [Title_Macro,InfoBox_Macro,Body_Macro,Reference_Macro,Category_Macro,External_links_Macro]
		listOfTokens = [self.title,self.Infobox,self.body,self.Reference,self.Category,self.External_links]
		sizeOfTokens = 0 

		if not self.docId % blockSizeLimit == 0:
			InMemoryInversion(listOfTokens, typesOfTokens, self.docId)
		else:
			writeBlocktoDisk()
			blockDict = {}
			InMemoryInversion(listOfTokens, typesOfTokens, self.docId)

"""##################################################################
Name : InMemoryInversion
Function :
	Creates an Inverted Index (Python Dict) in RAM
Args :
    listOfTokens : List of String field Info ,
    typesOfTokens: Type of Field Info 		 ,
    docId		 :	DocId
Returns:
    None
###################################################################"""

def InMemoryInversion(listOfTokens = None, typesOfTokens = None, docId = None):

	global blockDict
	for (tokenList,tokentype) in zip(listOfTokens,typesOfTokens):
			for token in [sn.stem(x) if tokentype == Body_Macro else x for x in re.findall('\w+',tokenList) if not x in stop_words and len(x) > 1]: # Also Try finditer 
				if not token in stop_words:
					try:
						if token != '' and blockDict[token]:
							try:
								if blockDict[token][docId]:
									try:
										if blockDict[token][docId][tokentype]:
											blockDict[token][docId][tokentype] += 1
									except KeyError:
										blockDict[token][docId][tokentype] = 1
									except:
										raise
							except KeyError:
								blockDict[token][docId] = {}
								blockDict[token][docId][tokentype] = 1
							except:
								raise
					except KeyError:
						blockDict[token] = {}
						blockDict[token][docId] = {}
						blockDict[token][docId][tokentype] = 1
					except:
						raise

"""##################################################################
Name : writeBlocktoDisk
Function : Stores glabal Dict in disk
Args :
    None
Returns:
    None
###################################################################"""
def writeBlocktoDisk():

	global blockDict
	global c
	bufferString = ''

	for token in sorted(list(blockDict.keys())):
		if token:
			bufferString += token + '~'
			for docId in blockDict[token].keys():
				bufferString += str(docId)
				for tokentype in blockDict[token][docId].keys():
					bufferString += str(tokentype) + str(blockDict[token][docId][tokentype])
				bufferString += '|'
			bufferString += '$'

	write_to_file(f'Chunk{c}.txt','w+',bufferString)

	c += 1

"""##############################################################
Name : parseXML
Function : Uses Elementree to parse XML dynamic fashion and uses
		   Page Parser to parse wikipage
Args :
    filename : Path to Wiki Dump
    blockSizeLimit : Number of keys per block file
Returns:
    None
###############################################################"""
def parseXML(filename = None, blockSizeLimit = None):

	global blockDict
	mapDocIdTitle = ''
	tree = ET.iterparse(filename,events=("start", "end"))
	pageId,pageText,pageTitle,docId,elemStack = '','','',0,[]
	for event,elem in tree:
		if event == 'end':
			# xmlStartTimer = time.time()
			if extractTag(elem.tag) == 'title':
				pageTitle = elem.text
			elif extractTag(elem.tag) == 'text':
				pageText = elem.text
				docId += 1
				parseObj = PageParser(docId,pageTitle)
				if pageText:
					if (docId % 1000) == 0:
						print('call spimiInvert on doc:',docId)
						# print(len(blockDict.keys()))
					# print(pageText)
					mapDocIdTitle += str(docId) + '~' + pageTitle + '|'
					parseObj.wikiParser(pageText)

			elem.clear()

	if blockDict:
		writeBlocktoDisk()
		blockDict = {}
	write_to_file('map_DocID_Title.txt','w+',mapDocIdTitle)


#******************* Main ******************#

if __name__ == '__main__':
	try:
		if sys.argv[2]:
			blockSizeLimit = int(sys.argv[2])
	except IndexError:
		blockSizeLimit = 30000
	except:
		raise
	filename = sys.argv[1]
	parseXML(filename,blockSizeLimit)





