import corpusParser
import sys
import GlbDef
import io

blockDict,c = {},1

def InMemoryInversion(listOfTokens = None, typesOfTokens = None, docId = None):

	global blockDict
	for (tokenList,tokentype) in zip(listOfTokens,typesOfTokens):
			for token in tokenList:
				# print(tokentype,token)
				try:
					if blockDict[token]:
						try:
							if blockDict[token][docId]:
								try:
									if blockDict[token][docId][tokentype]:
										blockDict[token][docId][tokentype] += 1
								except KeyError:
									blockDict[token][docId][tokentype] = 1
								except:
									raise
						except KeyError:
							blockDict[token][docId] = {}
							blockDict[token][docId][tokentype] = 1
						except:
							raise
				except KeyError:
					blockDict[token] = {}
					blockDict[token][docId] = {}
					blockDict[token][docId][tokentype] = 1
				except:
					raise

def spimiInvert(parseObj = None, blockSizeLimit = None, docId = None):
	
	global blockDict
	typesOfTokens = [GlbDef.Title,GlbDef.InfoBox,GlbDef.Body,GlbDef.Reference,GlbDef.Category,GlbDef.External_links]
	listOfTokens = [parseObj.title,parseObj.InfoBoxTokens,parseObj.body,parseObj.ReferenceTokens,parseObj.categoryList,parseObj.External_links]
	sizeOfTokens = 0 
	for x in listOfTokens:
		sizeOfTokens  += sys.getsizeof(x)
	if (sys.getsizeof(blockDict) + sizeOfTokens)/(1024*1024) < blockSizeLimit:
		InMemoryInversion(listOfTokens, typesOfTokens, docId)
	else:
		writeBlocktoDisk()
		blockDict.clear()
		InMemoryInversion(listOfTokens, typesOfTokens, docId)

def writeBlocktoDisk():

	global blockDict
	global c
	bufferString = ''
	for token in blockDict.keys():
		bufferString += token + ':'
		for docId in blockDict[token].keys():
			bufferString += str(docId)
			for tokentype in blockDict[token][docId].keys():
				bufferString += str(tokentype) + str(blockDict[token][docId][tokentype])
			bufferString += '|'
		bufferString += '$'

	with open(f'Chunk{c}.txt','w+') as fh:
		fh.write(bufferString)
	print(f'Chunk{c}.txt DONE')
	c += 1

def mergeVocab():
	pass

def compression():
	pass

if __name__ == '__main__':

	blockSizeLimit = int(sys.argv[2])
	filename = sys.argv[1]
	parseObj = corpusParser.parseAll(filename)
	parseObj.parseXML(blockSizeLimit)
