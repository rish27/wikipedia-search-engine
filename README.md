

# WikiPedia-Search-Engine
Goal: Retrieve Wikipedia pages related to the query and order them according to relevance

* Task 1: To create chunks of inverted index (sorted).
* Task 2: Enable Search Functionality. Rank the results based on tf-idf & cosine similarity metrics.

## Utility

* 2018201074/corpusParser.py- parses wikipedia dump and makes inverted index and merges index files and split them into smaller chunks
* 2018201074/search.py- main query program that returns results

## Data
Data used is entire wikipedia corpus which is passed in indexer, and indexed results are searched while queries.
[link](https://drive.google.com/file/d/1QMpM1CSn6j8Hwu5AabTqTQ1km9xCzSEV/view?usp=sharing)

## Steps

Click 'Download' on the top right hand side of the repository.

* Download the data from [here](https://drive.google.com/file/d/1QMpM1CSn6j8Hwu5AabTqTQ1km9xCzSEV/view?usp=sharing).

* Create a folder named Temp in WikiPedia Seach Engine folder.

* Run the following commands
```bash
python corpusParser.py <absolute path of data>
python search.py <absolute path of folder where indexed files stored>
```
* Fire queries to get search results.

**Queries** <br>
Normal search, give any words, phrases, sentences for search.

**Advanced search** <br>
Supports various field queries <br>
Use the following syntax for doing advanced search <br>
t: titlename b: words i: infobox r: references e: category  <br>

Example->  t: titlename would search the articles with <titlename>.


## Constructing the Inverted Index
* BasicStages(inorder):
* XML parsing: Elementree SAX parser used
* Data preprocessing 
  * Tokenization 
  * Case folding
  * Stop words removal
  * Stemming
* Posting List / Inverted Index Creation
* Optimize

## Features:
* Support for Field Queries . Fields include Title, Infobox, Body, Category, Links, and
References of a Wikipedia page. This helps when a user is interested in searching for
the movie ‘Up’ where he would like to see the page containing the word ‘Up’ in the title
and the word ‘Pixar’ in the Infobox. You can store field type along with the word when
you index.
* Index size should be less than 1⁄4 of dump size. 
* Scalable index construction 
* Search Functionality
secs.
  * Inverted index size: 1/4th of entire wikipedia corpus
* Advanced search as mentioned above.



## References
https://nlp.stanford.edu/IR-book/html/htmledition/boolean-retrieval-1.html
