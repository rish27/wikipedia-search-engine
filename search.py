import sys
import re
from nltk.stem.snowball import SnowballStemmer
sn = SnowballStemmer("english")

stop_words = {"it's", 'few', "hadn't", "haven't", 'ma', 'they', 'now', 'against', 'from', 'am', 'both', "mightn't", 'in', 'your', 'our', "aren't", 'hers', 'how', 'mustn', \
"isn't", 'ours', 'is', 'up', 'off', 'has', 'just', 'be', "needn't", 'down', 'why', 'some', "you've", 'most', 'an', 'where', 'weren', 'should', "won't", 're', 'any', \
'ourselves', 'had', 'for', 'isn', 'we', 'each', 'have', 've', "doesn't", 'wouldn', "wouldn't", 'she', 'more', 'he', 'it', "should've", 'to', 'over', 'does', 'whom', \
"don't", 'theirs', 'her', 'when', 'himself', 'don', 'during', 'i', 'between', 'own', 'not', 'did', 's', 'needn', "wasn't", 'me', 'before', 'if', 't', 'didn', 'very', \
'm', 'while', 'again', 'as', 'll', "hasn't", 'him', 'wasn', 'his', 'd', 'herself', 'my', 'will', 'by', 'were', 'ain', 'their', 'was', 'out', 'under', 'o', 'haven', \
'yourself', 'about', 'can', 'shouldn', 'a', 'mightn', 'the', 'after', "you're", 'this', 'on', 'yourselves', 'what', 'shan', 'aren', 'been', 'nor', "weren't", 'and', \
'such', 'there', 'same', 'itself', 'them', 'being', 'but', 'then', "you'd", 'until', 'that', 'those', 'myself', 'with', "shan't", 'you', 'themselves', 'its', 'do',\
 'than', 'hadn', "mustn't", "you'll", "that'll", 'of', 'through', 'which', 'so', 'yours', 'are', 'couldn', 'won', "shouldn't", 'these', 'too', "couldn't", 'below', \
 'hasn', 'doing', 'above', 'all', 'y', "she's", 'only', 'doesn', 'at', 'having', 'other', 'because', 'into', 'here', 'further', "didn't", 'no', 'once', 'who', 'or'}

mapTitleId = {}
memDict = {}

def read_file(testfile):
    with open(testfile, 'r') as file:
        queries = file.readlines()
    return queries

def write_file(outputs, path_to_output):
    '''outputs should be a list of lists.
        len(outputs) = number of queries
        Each element in outputs should be a list of titles corresponding to a particular query.'''
    with open(path_to_output, 'w') as file:
        for output in outputs:
            for line in output:
                file.write(line.strip() + '\n')
            file.write('\n')

# ================================================================================================ #

def parseExtract(text = None):
    
    resultDict = {}
    fieldWiseDocList = re.finditer('([0-9]+)([tibrlc][0-9]+)([tibrlc][0-9]+)?([tibrlc][0-9]+)?([tibrlc][0-9]+)?([tibrlc][0-9]+)?([tibrlc][0-9]+)?',text)

    for entry in fieldWiseDocList:
        docId = entry.group(1)
        for idx in range(2,7):
            field_Weight = entry.group(idx)
            if field_Weight:
                field = field_Weight[0]
                Weight = int(field_Weight[1:])
                resultDict[docId] = {}
                resultDict[docId][field] = Weight
            else:
                break
    return resultDict      

def queryParser(query):

    global memDict

    docSet = set([])
    isfieldQuery = re.match('^(title:|infobox:|body:|category:|ref:)',query)
    if not isfieldQuery:
        stemQuery = set([sn.stem(x) for x in re.findall('\w+',query.lower()) if not x in stop_words])
        lowerQuery = set(re.findall('\w+',query.lower()))
        lowerQuery = lowerQuery.union(stemQuery)
        for token in lowerQuery:
            # print(token)
            try:
                if memDict[token]:
                    # print(memDict)
                    if not isinstance(memDict[token],dict):
                        memDict[token] = parseExtract(memDict[token])
                        
                    if docSet:
                        docSet = docSet & set(memDict[token].keys())                   # Phase2 Note: optimisation required for intersection
                    else:
                        docSet = set(memDict[token].keys())
                    # print(docSet)
            except KeyError:
                continue
            except:
                raise
    else:
        fieldQuery = re.finditer('(title:|infobox:|body:|category:|ref:)([\w+\s+]+)(?=(title:|infobox:|body:|category:|ref:|$))',query.lower())

        for match in fieldQuery:
            field,text = match.group(0).split(':')
            stemQuery = set([sn.stem(x) if field == 'body' else x for x in re.findall('\w+',text) if not x in stop_words])

            for token in stemQuery:
                try:
                    if memDict[token]:
                        if not isinstance(memDict[token],dict):
                            memDict[token] = parseExtract(memDict[token])
                        # Intersection of docIds with field presence in their dictionary
                        if docSet:
                            docSet = docSet & set(memDict[token].keys())                   # Phase2 Note: optimisation required for intersection
                        else:
                            docSet = set(memDict[token].keys())
                    docList = list(docSet)
                    for docx in docList:
                        if not field[0] in memDict[token][docx].keys():
                            docList.remove(docx)
                except KeyError:
                    continue
                except Exception as e:
                    raise
            docSet = set(docList)
    return docSet

def each_chunk(stream = None, separator = None):
    buffer = ''
    while True:  # until EOF
        chunk = stream.read(4096)
        # print(chunk)
        if not chunk:
            yield buffer
            break
        buffer += chunk
        while True:  # until no separator is found
            try:
                part, buffer = buffer.split(separator, 1)
            except ValueError:
                break
            else:
                yield part

def loadMaptoMem(mapFileName = None):

    global mapTitleId
    with open(mapFileName,'r',encoding="utf-8") as fh:
        text = fh.read()

    parseMap = re.finditer('([0-9]+)~(.*?)(?:\|)',text)

    for x in parseMap:
        docId = x.group(1)
        title = x.group(2)
        mapTitleId[docId] = title

def search(path_to_index = None,indexFilename = None,mapFileName = None,queries = None):
    '''Write your code here'''
    global memDict
    path_to_index_file = path_to_index +f'/{indexFilename}'
    fh = open(path_to_index_file, "r", encoding="utf-8")

    for chunk in each_chunk(fh,'$'):
        # print(re.findall('[a-zA-Z0-9_]+',chunk))
        # print(chunk)
        if chunk:
            extract = chunk.split('~')
            keyword,docList = extract[0],extract[1]
            memDict[keyword] = docList
    fh.close()

    path_to_map =path_to_index + f'/{mapFileName}'
    loadMaptoMem(path_to_map)

    for line in queries:
        docSet = queryParser(line)
        for x in docSet:
            print(mapTitleId[x])
        print()
# =================================================================================================== #

def main():
    path_to_index = sys.argv[1]
    testfile = sys.argv[2]
    path_to_output = sys.argv[3]

    indexFilename = sys.argv[4]
    mapFileName = sys.argv[5]

    queries = read_file(testfile)
    outputs = search(path_to_index, indexFilename, mapFileName, queries)
    # write_file(outputs, path_to_output)


if __name__ == '__main__':
    main()
