import sys
import re
import Stemmer
import os
import pickle
import pprint
import math
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import time
import gc

sn = Stemmer.Stemmer('english')

stop_words = {"it's", 'few', "hadn't", "haven't", 'ma', 'they', 'now', 'against', 'from', 'am', 'both', "mightn't", 'in', 'your', 'our', "aren't", 'hers', 'how', 'mustn', \
"isn't", 'ours', 'is', 'up', 'off', 'has', 'just', 'be', "needn't", 'down', 'why', 'some', "you've", 'most', 'an', 'where', 'weren', 'should', "won't", 're', 'any', \
'ourselves', 'had', 'for', 'isn', 'we', 'each', 'have', 've', "doesn't", 'wouldn', "wouldn't", 'she', 'more', 'he', 'it', "should've", 'to', 'over', 'does', 'whom', \
"don't", 'theirs', 'her', 'when', 'himself', 'don', 'during', 'i', 'between', 'own', 'not', 'did', 's', 'needn', "wasn't", 'me', 'before', 'if', 't', 'didn', 'very', \
'm', 'while', 'again', 'as', 'll', "hasn't", 'him', 'wasn', 'his', 'd', 'herself', 'my', 'will', 'by', 'were', 'ain', 'their', 'was', 'out', 'under', 'o', 'haven', \
'yourself', 'about', 'can', 'shouldn', 'a', 'mightn', 'the', 'after', "you're", 'this', 'on', 'yourselves', 'what', 'shan', 'aren', 'been', 'nor', "weren't", 'and', \
'such', 'there', 'same', 'itself', 'them', 'being', 'but', 'then', "you'd", 'until', 'that', 'those', 'myself', 'with', "shan't", 'you', 'themselves', 'its', 'do',\
 'than', 'hadn', "mustn't", "you'll", "that'll", 'of', 'through', 'which', 'so', 'yours', 'are', 'couldn', 'won', "shouldn't", 'these', 'too', "couldn't", 'below', \
 'hasn', 'doing', 'above', 'all', 'y', "she's", 'only', 'doesn', 'at', 'having', 'other', 'because', 'into', 'here', 'further', "didn't", 'no', 'once', 'who', 'or'}

mapTitleId = {}
memDict = {}
mapDb = None
indexDb = None

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class MyFileError(LookupError):
    '''raise this file is not found'''

class MyListSizeError(LookupError):
    '''raise this when array size is small'''

def read_file(testfile):
    with open(testfile, 'r') as file:
        queries = file.readlines()
    return queries

def write_file(outputs, path_to_output):
    '''outputs should be a list of lists.
        len(outputs) = number of queries
        Each element in outputs should be a 
        list of titles corresponding to a particular query.
    '''
    with open(path_to_output, 'w') as file:
        for output in outputs:
            for line in output:
                file.write(line.strip() + '\n')
            file.write('\n')

# ================================================================================================ #

def parseExtract(text = None):
    '''Parse string to creating document list for term uses regex'''
    resultDict = {}
    fieldWiseDocList = re.finditer('([0-9]+)([tibrlc][0-9]+)([tibrlc][0-9]+)?([tibrlc][0-9]+)?([tibrlc][0-9]+)?([tibrlc][0-9]+)?([tibrlc][0-9]+)?',text)
    docsOfOccurence = 0
    for entry in fieldWiseDocList:
        docId = entry.group(1)
        tf = 0
        docsOfOccurence += 1
        for idx in range(2,7):
            field_Weight = entry.group(idx)
            if field_Weight:
                field = field_Weight[0]
                Weight = int(field_Weight[1:])
                resultDict[docId] = {}
                resultDict[docId][field] = Weight
                if field == 't':
                    tf += Weight/100
                elif field == 'i':
                    tf += Weight/100
                elif field == 'c':
                    tf += Weight/100
                elif field == 'r':
                    tf += Weight/100
                else:
                    tf += Weight/100
            else:
                break
        resultDict[docId]['f'] = tf

    idf = math.log(list(mapDb.keys())[-1]/docsOfOccurence,2)
    for docId in resultDict.keys():
        resultDict[docId]['f'] = resultDict[docId]['f'] * idf

    return resultDict,idf

def queryParser(query):
    '''
    check field query v/s normal query ,perform intersection on list of documents for each term if reduced to 0 then use previous document set
    '''
    global memDict

    prevDocSet,docSet,tf,temp = set([]),set([]),0,{}

    isfieldQuery = re.match('^(title:|infobox:|body:|category:|ref:)',query.lower())
    if not isfieldQuery:
        stemQuery = set([sn.stemWord(x) for x in re.findall('\w+',query.lower()) if not x in stop_words])
        lowerQuery = set(re.findall('\w+',query.lower()))
        lowerQuery = lowerQuery.union(stemQuery)
        initPop = False
        for token in lowerQuery:
            try:
                if memDict[token]:
                    pass
            except KeyError:
                search(token)
            except:
                raise
            try:
                if memDict[token]:
                    # print(memDict)
                    if not isinstance(memDict[token],dict):
                        memDict[token],idf = parseExtract(memDict[token])
                        
                    if initPop:
                        docSet = docSet & set(memDict[token].keys())                   # Phase2 Note: optimisation required for intersection
                        if not docSet:
                            docSet = prevDocSet
                    else:
                        docSet = set(memDict[token].keys())
                        initPop = True
                    # print(docSet)
                    prevDocSet = docSet
                    # print(token,idf)
            except KeyError:
                continue
            except:
                raise
        for docx in list(docSet):
            temp[docx] = 0
            for token in lowerQuery:
                try:
                    # print(token,docx,memDict[token][docx]['f'])
                    temp[docx] += memDict[token][docx]['f']
                except KeyError:
                    continue
                except:
                    raise
    else:
        # print(repr(query))
        fieldQuery = re.finditer('(title:|infobox:|body:|category:|ref:)([\w+\s+]+)(?=(title:|infobox:|body:|category:|ref:|$))',query.lower())
        initPop = False
        temp,docList,interimSet = {},[],set([])
        # print(fieldQuery.__next__())
        for match in fieldQuery:
            field,text = match.group(0).split(':')
            stemQuery = set([sn.stemWord(x) for x in re.findall('\w+',text.lower()) if not x in stop_words])
            lowerQuery = set(re.findall('\w+',query.lower()))
            lowerQuery = lowerQuery.union(stemQuery)

            for token in lowerQuery:
                try:
                    if memDict[token]:
                        pass
                        # print(memDict,token)
                except KeyError:
                    search(token)
                    # print(memDict,token)
                except:
                    raise
                try:
                    if memDict[token]:
                        if not isinstance(memDict[token],dict):
                            memDict[token],idf = parseExtract(memDict[token])
                            # print(memDict)
                        # Intersection of docIds with field presence in their dictionary
                        if initPop:
                            docSet = docSet & set(memDict[token].keys())                   # Phase2 Note: optimisation required for intersection
                            if not docSet:
                                docSet = prevDocSet | set(memDict[token].keys())
                        else:

                            # print(type(memDict[token][0]))
                            docSet = set(memDict[token].keys())
                            initPop = True
                        prevDocSet = docSet

                    for docx in docSet:
                        if field[0] in memDict[token][docx].keys():
                            # print(token,field,docx,memDict[token][docx])
                            # docList.remove(docx)
                            interimSet.add(docx)
                except KeyError:
                    continue
                except Exception as e:
                    raise

        for docx in interimSet:
            temp[docx] = 0
            for match in fieldQuery:
                field,text = match.group(0).split(':')
                stemQuery = set([sn.stemWord(x) if field == 'body' else x for x in re.findall('\w+',text) if not x in stop_words])
                for token in stemQuery:
                    temp[docx] += memDict[token][docx]['f']

    return sorted(temp.items(), key = lambda x : x[1],reverse = True)

def queryRefiner(query = None):

    isfieldQuery = re.match('^(title:|infobox:|body:|category:|ref:)',query.lower())
    if not isfieldQuery:
        return query
    else:
        newQuery,newQueryTitle = '',''
        fieldQuery = re.finditer('(title:|infobox:|body:|category:|ref:)([\w+\s+]+)(?=(title:|infobox:|body:|category:|ref:|$))',query.lower())
        for match in fieldQuery:
            field,text = match.group(0).split(':')
            if field[0] == 't':
                newQueryTitle = text
            else:
                newQuery = ''.join([newQuery,text])
        print("Field query",newQueryTitle,newQuery)
        return newQueryTitle + ' ' +newQuery

def display(docSet = None, query = None, tf_IdfedocCount = None):
    ''' Lists all the Title from tf-idf ranking docSet and the perform cosine similarity'''
    global mapDb

    seq = 1
    stemTitleList,plainTitleList,commitTitleList,resultNotOK = [],[],[],True
    print()
    refineQuery = queryRefiner(query.lower())
    # print(refineQuery)
    if docSet:
        fh = None
        while (resultNotOK):
            resultNotOK = False
            if len(docSet) < tf_IdfedocCount:
                tf_IdfedocCount = len(docSet)
            for x,score in docSet[:tf_IdfedocCount]:
                for lastDocId in mapDb.keys():
                    if int(x) <= int(lastDocId):
                        filename = mapDb[lastDocId].replace('/home/varun/Desktop/Project/Index_Database','Index_Database/MapDb')
                        break
                try:
                    fh = open(filename,'r')
                except FileNotFoundError:
                    raise 

                for line in fh:
                    extract = line.split('~')
                    docId,title = extract[0],extract[1]
                    stemmedTitle = ''
                    if x == docId:
                        for x in re.findall('[a-zA-Z0-9]+',title.lower()):
                            stemmedTitle = ' '.join([stemmedTitle,sn.stemWord(x)])
                        mapTitleId[x] = stemmedTitle
                        stemTitleList.append(stemmedTitle)
                        plainTitleList.append(title)
            stemmedQuery = ''
            for x in re.findall('[a-zA-Z0-9]+',refineQuery.lower()):
                stemmedQuery = ' '.join([stemmedQuery,sn.stemWord(x)])

            stemTitleList.append(stemmedQuery.lower())
            count_vectorizer = CountVectorizer(stop_words='english')
            count_vectorizer = CountVectorizer()
            sparse_matrix = count_vectorizer.fit_transform(stemTitleList)
            doc_term_matrix = sparse_matrix.todense()
            df = pd.DataFrame(doc_term_matrix)

            cosinescore = dict(zip(plainTitleList,cosine_similarity(df.tail(1), df)[-1]))

            for title,score in sorted(cosinescore.items(), key = lambda x : x[1],reverse = True):
                if seq == 7 and score == 0:
                    resultNotOK = True
                    tf_IdfedocCount = tf_IdfedocCount * 10
                    print(bcolors.BOLD + bcolors.WARNING + "Source Re Selection for better results.This might take few seconds..." + bcolors.ENDC)
                    print()
                    seq = 1
                    break
                if seq <= 10:
                    commitTitleList.append(title)
                    # print(bcolors.BOLD + str(seq) + '.' + title)
                else:
                    break
                seq +=1
            if len(docSet) < tf_IdfedocCount or (tf_IdfedocCount % 1000 == 0):
                if commitTitleList:
                    seq = 1
                    break
                else:
                    raise MyListSizeError(' Title List seems empty')
            if resultNotOK:
                commitTitleList = []
            stemTitleList,plainTitleList = [],[]
            seq = 1
        for title in commitTitleList:
            print(bcolors.BOLD + str(seq) + '.' + title)
            seq += 1

        fh.close()
    else:
        print('Sorry No Results Found')



def search(token = None):
    '''find correct index file '''
    global memDict
    global mapDb
    global indexDb

    fh = None
    for word in indexDb.keys():
        if token <= word:
            # print(token,word)
            # filename = indexDb[word].replace('/home/varun/Desktop/Project/Index_Database/','Index_Database')
            filename = os.path.join('Index_Database/MergeDb',os.path.basename(indexDb[word]))
            break
    # print(os.getcwd())
    # print(os.listdir())
    try:   
        fh = open(filename,'r')
    except FileNotFoundError:
        raise MyFileError(f'Known Issue : {filename} Not Found Please download from iCloud')
    except:
        raise
    
    for line in fh:
        extract = line.split('~')
        docList = ''
        try:
            keyword,docList = extract[0],extract[1]         # Get Word ~ docId1Field|docId2Field|docId3Field.....
        except:
            print(extract)
            raise
        if token == keyword:
            memDict[keyword] = docList
    fh.close()
    # print(memDict)
    
# =================================================================================================== #

def main():

    global indexDb
    global mapDb
    path_to_index = sys.argv[1]
    path_to_map = sys.argv[2]

    # testfile = sys.argv[2]
    # path_to_output = sys.argv[3]
    indexFilename = "IndexPickle"
    mapFileName = "MapPickle"
    indexfh = open(os.path.join(path_to_index,indexFilename), 'rb')
    mapfh = open(os.path.join(path_to_map,mapFileName),'rb')
    indexDb = pickle.load(indexfh)
    mapDb = pickle.load(mapfh)

    print(" Type exit to end program ")
    # queries = read_file(testfile)
    countOf_Tf_idf_docx = 50
    result = []
    while True:
        print()
        print(bcolors.HEADER + bcolors.BOLD +'Search for : '+ bcolors.ENDC,end = '')
        query = input()
        starttime = time.time()
        if query == 'exit':
            gc.collect()
            break
        docSet = queryParser(query)
        temp = []
        # print(docSet)
        display(docSet,query,countOf_Tf_idf_docx)
        print('Time Elaspsed: ',time.time() - starttime)
        gc.collect()

    indexfh.close()
    mapfh.close()


if __name__ == '__main__':
    main()
