# -*- coding: utf-8 -*-
"""
This module parses Wiki XML corpus,performs Tokenization and store parsed data in file

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections.

        $ python corpusParser.py

Attributes:

Todo:
    * For module TODOs

"""
import xml.etree.cElementTree as ET
import re
# from nltk.tokenize import word_tokenize
# from nltk.stem.snowball import SnowballStemmer
import Stemmer
import sys
import time
import string
import io
from collections import deque
import heapq
from os import path
import os
import pickle
import pathlib

stop_words = {"it's", 'few', "hadn't", "haven't", 'ma', 'they', 'now', 'against', 'from', 'am', 'both', "mightn't", 'in', 'your', 'our', "aren't", 'hers', 'how', 'mustn', \
"isn't", 'ours', 'is', 'up', 'off', 'has', 'just', 'be', "needn't", 'down', 'why', 'some', "you've", 'most', 'an', 'where', 'weren', 'should', "won't", 're', 'any', \
'ourselves', 'had', 'for', 'isn', 'we', 'each', 'have', 've', "doesn't", 'wouldn', "wouldn't", 'she', 'more', 'he', 'it', "should've", 'to', 'over', 'does', 'whom', \
"don't", 'theirs', 'her', 'when', 'himself', 'don', 'during', 'i', 'between', 'own', 'not', 'did', 's', 'needn', "wasn't", 'me', 'before', 'if', 't', 'didn', 'very', \
'm', 'while', 'again', 'as', 'll', "hasn't", 'him', 'wasn', 'his', 'd', 'herself', 'my', 'will', 'by', 'were', 'ain', 'their', 'was', 'out', 'under', 'o', 'haven', \
'yourself', 'about', 'can', 'shouldn', 'a', 'mightn', 'the', 'after', "you're", 'this', 'on', 'yourselves', 'what', 'shan', 'aren', 'been', 'nor', "weren't", 'and', \
'such', 'there', 'same', 'itself', 'them', 'being', 'but', 'then', "you'd", 'until', 'that', 'those', 'myself', 'with', "shan't", 'you', 'themselves', 'its', 'do',\
 'than', 'hadn', "mustn't", "you'll", "that'll", 'of', 'through', 'which', 'so', 'yours', 'are', 'couldn', 'won', "shouldn't", 'these', 'too', "couldn't", 'below', \
 'hasn', 'doing', 'above', 'all', 'y', "she's", 'only', 'doesn', 'at', 'having', 'other', 'because', 'into', 'here', 'further', "didn't", 'no', 'once', 'who', 'or'}
sn = Stemmer.Stemmer('english')

Title_Macro = 't'
InfoBox_Macro = 'i'
Body_Macro = 'b'
Reference_Macro = 'r'
Category_Macro = 'c'
External_links_Macro = 'l'
blockDict,blocKeyCount,c,KEYS_MAX = {},{},0,0

def extractTag(str = None):
	'''parsed text from XML <title> tag'''
	if str == None:
		return ''
	else:
		expr = re.compile('{(.*)}(.*)').match(str)
		return expr.group(2)


def write_to_file(filename = None, mode = None,bufferString = None):

	with open(filename,mode) as fh:
		fh.write(bufferString)
		print(f"{filename} write complete")

class PageParser(object):
	"""docstring for PageParser"""
	def __init__(self,docId = None ,parseTitle = None):
		super(PageParser, self).__init__()
		self.title = parseTitle.lower()
		self.Infobox = ''
		self.body = ''
		self.Reference = ''
		self.External_links = ''
		self.Category = ''
		self.docId = docId

	def wikiParser(self,pageText = None, outputPath = None):
		'''parse text in <text> and calls spimiInvert'''
		flagInfobox,flagReference,flagExternal,flagCategory,flagExtExcep = False,False,False,False,False
		infoboxStack,refStack,extStack,catStack = 0,0,0,0
		strBufferInfoBox,strBufferRef,strBufferExternal,strBufferCat,strBuffBody = '','','','',''
		outlinks = []
		for line in io.StringIO(pageText):

			line = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+','',line)
			if re.search('\{\{\s*Infobox',line):
				flagInfobox = True
				strBufferInfoBox = ''.join([strBufferInfoBox,' ',line])
				infoboxStack = 1
				# print("flag info set")
			elif re.search('==\s*references\s*==',line.lower()):
				flagReference = True
				flagExternal = False
				flagCategory = False
			elif re.search('==\s*external links\s*==',line.lower()):
				flagExternal = True
				flagReference = False
				flagCategory = False
			elif re.search('\[\[Category:|\[\[:Category:',line):
				flagCategory = True
				flagReference = False
				flagExternal = False
				exp = re.match('\[\[Category:(.+)\]\]|\[\[:Category:(.+)\]\]',line)
				if exp and exp.group(1):
					strBufferCat = ''.join([strBufferCat,' ',exp.group(1)])
				elif exp and exp.group(2):
					strBufferCat = ''.join([strBufferCat,' ',exp.group(2)])
			elif flagInfobox:
				openBr = re.findall('\[\[|\{\{',line)
				closeBr = re.findall('\]\]|\}\}',line)
				if openBr:
					infoboxStack += len(openBr)
				if closeBr:
					infoboxStack -= len(closeBr)
				if infoboxStack == 0:
					flagInfobox = False
				# outlinks.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+',line))
				# for link in outlinks:
				# 	line = re.sub(re.escape(link),' ',(line))
				strBufferInfoBox = ''.join([strBufferInfoBox,' ',line])
			elif flagReference:
				# outlinks.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+',line))
				# for link in outlinks:
				# 	line = re.sub(re.escape(link),' ',line)
				exp = re.match('\{\{(.*)\}\}',line)
				if exp and exp.group(1):
					line = exp.group(1)
				strBufferRef = ''.join([strBufferRef,' ',line])
			elif flagExternal:
				exp = re.match('\*\s*\{\{(.*)\}\}|\*\s\[(.*)\]',line)
				if exp and exp.group(1):
					line = exp.group(1)
				elif exp and exp.group(2):
					line = exp.group(2)
				strBufferExternal = ''.join([strBufferExternal,' ',line])
			else:
				# outlinks.extend(re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+])+',line))
				# for link in outlinks:
				# 	line = re.sub(re.escape(link),' ',(line))
				strBuffBody = ''.join([strBuffBody,' ',line])
		pageText = ''
		self.Infobox = re.sub('<.*?>','',strBufferInfoBox.lower())
		self.Reference = re.sub('<.*?>','',strBufferRef.lower())
		self.External_links = re.sub('<.*?>','',strBufferExternal.lower())
		self.body = re.sub('<.*?>','',strBuffBody.lower())

		self.Category = re.sub('<.*?>','',strBufferCat.lower())

		self.spimiInvert(blockSizeLimit,outputPath)

	def spimiInvert(self, blockSizeLimit = None,outputPath = None):
		'''parse XML dump and call parseWiki() to parse text in <text>'''
		global blockDict
		typesOfTokens = [Title_Macro,InfoBox_Macro,Body_Macro,Reference_Macro,Category_Macro,External_links_Macro]
		listOfTokens = [self.title,self.Infobox,self.body,self.Reference,self.Category,self.External_links]
		# sizeOfTokens = 0 

		if not self.docId % blockSizeLimit == 0:
			InMemoryInversion(listOfTokens, typesOfTokens, self.docId)
		else:
			writeBlocktoDisk(outputPath)
			blockDict = {}
			InMemoryInversion(listOfTokens, typesOfTokens, self.docId)


def InMemoryInversion(listOfTokens = None, typesOfTokens = None, docId = None):
	'''
	Creates an Inverted Index (Python Dict) in RAM

	Keyword arguments:
    	 listOfTokens  : List of String field Info 
    	 typesOfTokens : Type of Field Info
    	 docId 		   : DocId
	'''
	global blockDict
	for (tokenList,tokentype) in zip(listOfTokens,typesOfTokens):
			for token in [sn.stemWord(x) if tokentype == Body_Macro else x for x in re.findall('[a-zA-Z0-9]+',tokenList) if not x in stop_words and len(x) > 1]: # Also Try finditer 
				if not token in stop_words:
					try:
						if token != '' and blockDict[token]:
							try:
								if blockDict[token][docId]:
									try:
										if blockDict[token][docId][tokentype]:
											blockDict[token][docId][tokentype] += 1
									except KeyError:
										blockDict[token][docId][tokentype] = 1
									except:
										raise
							except KeyError:
								blockDict[token][docId] = {}
								blockDict[token][docId][tokentype] = 1
							except:
								raise
					except KeyError:
						blockDict[token] = {}
						blockDict[token][docId] = {}
						blockDict[token][docId][tokentype] = 1
					except:
						raise

def writeBlocktoDisk(outputPath = None):
	'''Write BlocDictionary to Disk'''
	global blockDict
	global c
	global KEYS_MAX
	bufferString = ''

	sizeOfDict = len(blockDict)
	if KEYS_MAX < sizeOfDict:
		KEYS_MAX = sizeOfDict
	for token in sorted(list(blockDict.keys())):
		if token:
			bufferString += token + '~'
			for docId in blockDict[token].keys():
				bufferString += str(docId)
				for tokentype in blockDict[token][docId].keys():
					bufferString += str(tokentype) + str(blockDict[token][docId][tokentype])
				bufferString += '|'
			bufferString += '\n'

	c += 1
	# print(outputPath,f'1_{c}.txt')
	filename = path.join(outputPath,f'1_{c}.txt')
	write_to_file(filename,'w+',bufferString)
	blocKeyCount[filename] = sizeOfDict
	
'''__________________ K WAY MERGE _____________________'''

def writeMergedBlocktoDisk(outputPath = None, resultIndex = None, mode = None):
	'''docstring for Write Merged Block To Disk'''
	global blocKeyCount
	bufferString = ''
	# tokenCount = 0
	for token in list(resultIndex.keys()):
		bufferString += token + '~'
		bufferString += resultIndex[token]
		bufferString += '\n'

	write_to_file(outputPath,mode,bufferString)
	# try:
	# 	if blocKeyCount[outputPath]:
	# 		blocKeyCount[outputPath] += len(resultIndex.keys())
	# except KeyError:
	# 	blocKeyCount[outputPath] = len(resultIndex.keys())

class compareHeapNode(object):
	"""docstring for compareHeapNode"""
	def __init__(self, keyword,doclist,file_handle,seq):
		super(compareHeapNode, self).__init__()
		self.keyword = keyword
		self.doclist = doclist
		self.file_handle = file_handle
		self.seq = seq

	def __lt__(self,other):
		if self.keyword < other.keyword:
			return True
		elif self.keyword == other.keyword:
			if self.seq < other.seq:
				return True
		return False

def K_Way(outputPath = None):
	'''Parallel K way Merge'''
	global KEYS_MAX
	Countkeys,hpQueue,resultIndex,mergeDirectory,mergePieceCount = 0,[],{},{},1

	for i in range(1,c+1):
		fh = open(path.join(outputPath,f'1_{i}.txt'),'r')
		extract = fh.readline()
		keyword,doclist = extract.split('~')
		obj = compareHeapNode(keyword,doclist,fh,i)
		heapq.heappush(hpQueue,obj)

	# print(q[0].doclist)
	while hpQueue:
		minObj = heapq.heappop(hpQueue)
		sKeyword, sDoclist, sSeq, sFileHandle = minObj.keyword, minObj.doclist, minObj.seq, minObj.file_handle
		try:
			if resultIndex[sKeyword]:
				resultIndex[sKeyword] = ''.join([resultIndex[sKeyword],sDoclist.strip()])
		except KeyError:
			resultIndex[sKeyword] = sDoclist.strip()
		except:
			raise
		extract = sFileHandle.readline()
		if extract:
			thisKey,thisDoclist = extract.split('~')
			thisObj = compareHeapNode(thisKey,thisDoclist,sFileHandle,sSeq)
			heapq.heappush(hpQueue,thisObj)

		Countkeys += 1
		if Countkeys > KEYS_MAX:
			pathToIndex = path.join(outputPath + '/MergeDb',f'2_{mergePieceCount}.txt')
			print(outputPath,pathToIndex)
			writeMergedBlocktoDisk(pathToIndex,resultIndex,'w+')
			mergePieceCount += 1
			Countkeys = 0
			resultIndex = {}
			mergeDirectory[sKeyword] = pathToIndex

	if resultIndex:
		pathToIndex = path.join(outputPath + '/MergeDb',f'2_{mergePieceCount}.txt')
		writeMergedBlocktoDisk(pathToIndex,resultIndex,'w+')
		mergeDirectory[sKeyword] = pathToIndex
	pickleDb = open(path.join(outputPath,'IndexPickle'),'ab')
	pickle.dump(mergeDirectory, pickleDb)
	pickleDb.close()

def parseXML(filename = None,outputPath = None ,blockSizeLimit = None):
	'''Uses Elementree to parse XML dynamic fashion and uses Page Parser to parse wikipage'''
	global blockDict
	mapDocIdTitle,blockCount,mapDirectory = '',1,{}
	tree = ET.iterparse(filename,events=("start", "end"))
	pageId,pageText,pageTitle,docId,elemStack = '','','',0,[]
	for event,elem in tree:
		if event == 'end':
			# xmlStartTimer = time.time()
			if extractTag(elem.tag) == 'title':
				pageTitle = elem.text
			elif extractTag(elem.tag) == 'text':
				pageText = elem.text
				docId += 1
				parseObj = PageParser(docId,pageTitle)
				if pageText:
					# print(pageText)
					mapDocIdTitle += str(docId) + '~' + pageTitle + '\n'
					parseObj.wikiParser(pageText,outputPath)
					if (docId % blockSizeLimit) == 0:
						pathToMap = path.join(outputPath,f'Map_{blockCount}.txt')
						write_to_file(pathToMap,'w+',mapDocIdTitle)
						mapDocIdTitle = ''
						blockCount += 1
						mapDirectory[docId] = pathToMap
						# print('call spimiInvert on doc:',docId)
						# print(len(blockDict.keys()))
			elem.clear()

	if blockDict:
		writeBlocktoDisk(outputPath)
		blockDict = {}
	if mapDocIdTitle:
		pathToMap = path.join(outputPath,f'Map_{blockCount}.txt')
		write_to_file(pathToMap,'w+',mapDocIdTitle)
		mapDirectory[docId] = pathToMap
	pickleDb = open(path.join(outputPath,'MapPickle'),'ab')
	pickle.dump(mapDirectory, pickleDb)
	pickleDb.close() 
	K_Way(outputPath)

#******************* Main ******************#

if __name__ == '__main__':

	try:
		if sys.argv[3]:							# preferably 20000 on 75 GB corpus
			blockSizeLimit = int(sys.argv[3])
	except IndexError:
		blockSizeLimit = 30000
	except:
		raise

	try:
		if sys.argv[4]:							# preferably 1000000 on 75 GB corpus
			KEYS_MAX = int(sys.argv[4])
	except IndexError:
		KEYS_MAX = 100000
	except:
		raise

	filename = sys.argv[1]
	outputPath = sys.argv[2]
	parseXML(filename,outputPath,blockSizeLimit)





